import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.PartitionInfo;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class EswarKafkaConsumer {


    public static void main(String args[]) {

        Map<String, List<PartitionInfo>> topics;

        Properties props = new Properties();
        props.put("bootstrap.servers", args[0]);
        props.put("group.id", "test-consumer-group");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");

        KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(props);
        topics = consumer.listTopics();
        List ignoredTopics = new ArrayList(Arrays.asList("heartbeats", "mm2-offset-syncs..internal", "mm2-configs.PRIMARY_CONSUMER_CLUSTER.internal", "mm2-offsets.PRIMARY_CONSUMER_CLUSTER.internal", ".checkpoints.internal", "mm2-status.PRIMARY_CONSUMER_CLUSTER.internal", "__consumer_offsets"));
        List<String> topics1 = new ArrayList<String>();
        for (String s : topics.keySet()) {

            if (ignoredTopics.contains(s)) {
                continue;
            }

            System.out.println(s);
            topics1.add(s);


        }


        consumer.close();


        int numConsumers = 3;
        String groupId = "test1";

        final ExecutorService executor = Executors.newFixedThreadPool(numConsumers);

        final List<ConsumerLoop> consumers = new ArrayList();
        for (int i = 0; i < numConsumers; i++) {
            ConsumerLoop consumer1 = new ConsumerLoop(i, groupId, topics1,args[0]);
            consumers.add(consumer1);
            executor.submit(consumer1);
        }

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                for (ConsumerLoop consumer : consumers) {
                    consumer.shutdown();
                }
                executor.shutdown();
                try {
                    executor.awaitTermination(5000, TimeUnit.MILLISECONDS);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

    }

}
